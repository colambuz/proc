#!/bin/sh

echo -e "PID\tTTY\tSTAT\tCOMMAND"

for i in $(ls -d /proc/* | grep -Eo "[0-9]{1,9}" | sort -un)
do
#Check if PID still exist
rule=$(cat /proc/$i/stat 2>/dev/null)
if [ $? = 0 ]; then

#PID
pid=$(cat /proc/$i/stat | awk '{print $1}')

#TTY
tty1=$(cat /proc/$i/stat | awk '{print $7}')
tty2=$(ls -l /proc/$i/fd | grep dev | cut -d\/ -f3,4 | uniq)
tty="?"
if [ $tty1 -ne 0 ]; then
  tty=$tty2
fi

#STAT
#(18)priority;(6)session;(20)num_threads;(8)tpgid
lo=$(cat /proc/$i/smaps | grep VmFlags | grep lo | wc -l)
stat=$(cat /proc/$i/stat | (awk '{printf $3; \
        if ($18 < 20) printf "<"; else if ($18>20) printf "N"; \
        if ($6 == $1) printf "s"; \
        if ($20 > 1) printf "l"; \
        if ($8 == $1) printf "+"}'; \
        if [ $lo -ne 0 ]; then printf "L"
        fi))

#TIME
#(14)utime;(15)stime
time=$(cat /proc/$i/stat | awk '{print strftime ("%M:%S", ($14+$15)/100)}')

#COMMAND
#comm1=$(cat /proc/$i/stat | awk '{print $2}')
comm2=$(cat /proc/$i/cmdline | tr '\000' ' ')
comm3=$(cat /proc/$i/comm)
chk=$(cat /proc/$i/cmdline | wc -w)

if [ $chk = 1 ]; then
  comm=$comm2
else
  comm="["$comm3"]"
fi
  echo -e $pid"\t"$tty"\t"$stat"\t"$time"\t"$comm"\t" 2>/dev/null

fi
done
